MDS, = glob_wildcards("{mdFileName}.md")
MDS.remove("README")

rule all:
	input: expand("{mdFileName}.pdf", mdFileName=MDS)

rule pdf:
	input: '{mdFile}.md'
	output: '{mdFile}.pdf'
	shell: 'pandoc {input} --number-sections --toc --listings -M date="$(date "+%B %e, %Y")"  -s -o {output}'


# content of the original generatePdf file
#
# pandoc coursGit.md --number-sections --toc --listings -s -o coursGit.pdf
# pandoc autoEvaluationGit.md --number-sections --toc --listings -s -o autoEvaluationGit.pdf
