% Auto-évaluation git


# Principes

- [ ] À quel(s) besoin(s) répond la gestion de versions (*version control* en anglais) ?
- [ ] Définir :
    - [ ] working directory
    - [ ] index (staging area)
    - [ ] head


# Manipulations simples

- [ ] Créer un projet (git init)
- [ ] Cloner un projet (git clone)
- [ ] Gérer les versions successives des fichiers du projet
    - [ ] git add
    - [ ] git commit
- [ ] Examiner le dépôt
    - git status
- [ ] Comparer le working directory, l'index et le head
    - [ ] git diff (working directory vs index)
    - [ ] git diff --cached (index vs head)
- [ ] Branches
    - [ ] lister les branches disponibles et repérer la branche active
    - [ ] créer une nouvelle branche (git checkout -b nomNouvelleBranche)
    - [ ] changer de branche active (git checkout nomBranche)
    - [ ] fusionner une branche avec la branche courante (git merge nomBrancheAFusionner)
    - [ ] résoudre d'éventuels conflits lors de la fusion de branches
- [ ] Synchroniser un projet cloné avec le projet original
    - [ ] reporter dans le projet cloné les mises à jour (de la branche courante) du projet original (git pull)
    - [ ] envoyer vers le projet original une branche du projet cloné (git push nomBrancheAEnvoyer)
