Ce projet contient le cours sur git.

Il complète le [git simple tutorial](https://gitlab.com/odameron/git-simple-tutorial/)

# Contenu
- `coursGit.md` et `coursGit.pdf` : trame (grandes parties et successions d'étapes à effectuer) pour le cours sur git
- `autoEvaluation.md`et `autoEvaluation.pdf` : auto-évaluation pour vérifier que vous êtes bien familier.e avec les principales notions de git
- `Snakefile` snakemake file for converting the markdown files into pdf

# Liens externes

(Les éléments de la liste ci-dessous sont des liens hypertextes)

- [git simple tutorial](https://gitlab.com/odameron/git-simple-tutorial/) (**Commencez par ici**, c'est le détail des étapes présentées dans `coursGit.md`)
- [bioinfo-fr.net : premiers pas avec git](https://bioinfo-fr.net/git-premiers-pas)
- [bioinfo-fr.net : cloner un projet, travailler à plusieurs, créer des branches](https://bioinfo-fr.net/git-usage-collaboratif)
