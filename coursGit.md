% Cours git

# Pré-requis ; ressources

(Les éléments de la liste ci-dessous sont des liens hypertextes)

- [bioinfo-fr.net : premiers pas avec git](https://bioinfo-fr.net/git-premiers-pas)
- [bioinfo-fr.net : cloner un projet, travailler à plusieurs, créer des branches](https://bioinfo-fr.net/git-usage-collaboratif)
- [git simple tutorial](https://gitlab.com/odameron/git-simple-tutorial/)

# Besoins justifiant l'utilisation de logiciels de contrôle de versions

- gérer les version successives
    - distinguer les versions majeures des versions intermédiaires
    - identifier la version la plus à jours
    - identifier la dernière version stable
    - comparer ce qui a changé entre deux versions (pas forcément successives)
- l'enchaînement des versions n'est pas toujours linéaire
- gérer les versions de plusieurs documents (on ne peut pas simplement se ramener au cas d'un seul fichier en utilisant une archive compressée)
- gérer l'aspect collaboratif


# Configurer git

- git init
    - observer le rep caché .git et ce qu'il contient
- git status
    - fonctionnement : cherche un rep .git dans le répertoire courant ; s'il n'en trouve pas, remonte jusqu'à en trouver un
    - essayer ```git status``` dansd un rep qui n'est pas un projet git (ou un des ses sous-répertoires)


# Utilisation simple de git

- git status
- créer un ficher (non vide)
- git status
- git add
    - ajoute à l'index
- git commit
    - copie l'index dans le head

**Bonne pratique :**
- faire des commit atomiques et simples, touchant le minimum de fichiers
- renseigner le message du commit de façon brève et informative


## Illustration de l'index et du head

Lors de la création d'un projet, le working directory, l'index et le head sont les mêmes.

### Mise à jour de l'index avant le commit

- git status
- créer un fichier
- git status
- git add
- modifier le fichier
- git status
    - l'index n'est plus à jour
- git add
- git commit # on a résolu le fait que l'index n'était plus à jour par rapport au working directory en faisant un nouveau git add

### Le commit copie l'index

- git status
- modifier le fichier
- git status
- git add
- modifier le fichier
- git commit

- vérifier avec un git clone que le nouveau head ne contient pas la dernière version du fichier
- git checkout [files]  restaure le working directory avec le contenu de l'index (mais n'efface pas d'éventuels nouveaux fichiers ; compléter avec git clean si besoin)


## git log = historique des commits


## git diff

- `git diff` compare la version courante avec la dernière version de l'index
- `git diff --cached` compare l'index avec le head
    - c'est ce qui va être modifié lors du prochain commit par rapport au dernier commit
- on ne compare jamais la version courante du fichier avec celle du dernier commit


## Fichier .gitignore

Contient des noms de fichiers ou des regex

- fichiers intermédiaires
- fichier de log
- fichiers binaires ou compressés


Il est pris en compte systématiquement. En général, on l'ajoute au projet avec un git add


# Gérer plusieurs branches sur un projet

- git branch
- git checkout -b : crée une nouvelle branche (qui deviant la branche active)
- git checkout : pour alterner entre les branches


## créer une branche, modifier un fichier puis la merger avec la branche master

- git status
- git branch
- git checkout -b maNouvelleBranche
- git branch
- modifier un fichier (il faut faire git add ET git commit)
- git add + git commit
- cat fichier # la modif est visible
- git checkout master
- cat fichier # la modif est invisible

- git merge maNouvelleBranche


## divergence et résolution de conflits

- git branch
- git checkout master
- modifier le fichier
- git add
- git commit -m "modif depuis master"
- git checkout maNouvelleBranche
- modifier le fichier (différemment que depuis la branche master)
- git add
- git commit
- git checkout master
- git merge maNouvelleBranche
- éditer le fichier et corriger le conflit
-


# Synchronisation de plusieurs versions du même dépôt

- git clone
- git pull
    - retrieves the remote head (may require to resolve conflicts)
- git push
    - propagates the local head to the remote head


## Propager les modifications du projet original vers le projet cloné

- créer un projet
- le cloner
- comparer les .git/config
- faire un nouveau commit sur le projet initial
- git status dans le projet cloné
- git pull

## Propager les modifications du projet cloné vers le projet original

- créer une branche depuis le projet cloné
- git branch
- git checkout -b "brancheClonee"
- git branch
- modifier fichier
- git add
- git commit -m "modif depuis brancheClonee"
- git push origin brancheClonee
- aller dans le projet original
- git branch
- git checkout master
- git merge brancheClonee


# Utiliser gitlab

# Repérer les commit qui ont modifié une ligne (ou une région) d'un fichier

## Tous les commits où cette ligne a été modifiée : `git log`

`git log -L 4,5:fichier1.txt` donne tous les commits qui ont changé la ligne 4.

## Le dernier commit où cette ligne a été modifiée : `git blame`

`git blame -L 4,5 fichier1.txt` ne donne que le dernier commit qui a changé cette ligne.
